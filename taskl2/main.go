// Исключительно на GO.
// Сделать обход каталога большой вложенности с большим объемом файлов (1млрд)
// не нагружающий дисковое IO с минимальными затратами ресурсов. Результат записать в файл (результат обхода каталога)
// Работу написанного вами кода проверить в своей тестовой среде. Необходимо убедиться, что данное решение можно катить в прод,
// абсолютно все условия задания выполнены, ваш код никак не навредит работе сервера.
package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"sync"
)

var (
	dirFlag      = flag.String("dir", ".", "path to the directory to traverse")
	outputFile   = flag.String("output", "result.txt", "name of the output file")
	fileListChan = make(chan string)
	workersCount = 5
	wg           sync.WaitGroup
	mu           sync.Mutex
)

func main() {
	flag.Parse()

	if *dirFlag == "" {
		fmt.Println("you must specify a directory path using the -dir flag")
		return
	}

	file, err := os.Create(*outputFile)
	if err != nil {
		fmt.Println("error creating the file:", err)
		return
	}
	defer file.Close()

	go func() {
		filepath.Walk(*dirFlag, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				fmt.Println("Error walking the directory:", err)
				return nil
			}

			if !info.IsDir() {
				fileListChan <- path
			}
			return nil
		})
		close(fileListChan)
	}()

	// goroutines to process results
	for i := 0; i < workersCount; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for path := range fileListChan {
				mu.Lock()
				_, err := file.WriteString(path + "\n")
				mu.Unlock()
				if err != nil {
					fmt.Println("error writing to the file:", err)
				}
			}
		}()
	}

	wg.Wait()
}
