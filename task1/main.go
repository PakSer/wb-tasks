// Посчитать сумму всех правых листьев бинарного дерева
package main

import "fmt"

type Node struct {
	Value int
	Left  *Node
	Right *Node
}

func sumRightLeaves(root *Node) int {
	if root == nil {
		return 0
	}
	sumRight := 0
	if root.Right != nil && root.Right.Left == nil && root.Right.Right == nil {
		sumRight += root.Right.Value
	} else {
		sumRight += sumRightLeaves(root.Right)
	}
	sumRight += sumRightLeaves(root.Left)

	return sumRight
}

func main() {
	root := &Node{Value: 8}
	root.Left = &Node{Value: 4}
	root.Right = &Node{Value: 16}
	root.Left.Left = &Node{Value: 3}
	root.Left.Right = &Node{Value: 7}
	root.Right.Left = &Node{Value: 12}
	root.Right.Right = &Node{Value: 18}

	result := sumRightLeaves(root)
	fmt.Printf("Сумма правых листьев бинарного дерева: %d", result)
}
